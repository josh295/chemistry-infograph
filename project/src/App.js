import React , { useState } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Nav from "./components/nav.js";
import Home from "./components/Home.js";
import ElementsInMe from "./components/Elements in me/Elements in me.js";
import ElementsOnMe from './components/Elements on me/Elements on me.js';
import ElementsAroundMe from "./components/Elements around me/Elements around me.js";
import Footer from "./components/Footer.js";
import "./components/Footer.css";
import "./App.css";
import References from './components/References.js';

function App() {
  const btnImgSrc = "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fcdn.iphonehacks.com%2Fwp-content%2Fuploads%2F2016%2F08%2Ficon.512x512-75-512x510.png&f=1&nofb=1";
  const [light , setLight] = useState ("mode");
  const toggleMode = () => {
    if (light === "mode") {
      setLight ("darkMode")
    }
    else {
      setLight ("mode")
    }
  }

  return (
    <>
      <BrowserRouter>
        <section  id = "navandcontent" className = {light}>
          <div id = "navDiv" >
            <Nav></Nav>
          </div>
          <div id = "contentDiv">
            <Switch>
            <Route exact path = "/" component = {Home}></Route>
            <Route path = "/ElementsInMe" component = {ElementsInMe}></Route>
            <Route path = "/ElementsOnMe" component = {ElementsOnMe}></Route>
            <Route path = "/ElementsAroundMe" component = {ElementsAroundMe}></Route>
            <Route path = "/References" component = {References}></Route>
            </Switch>
            <Footer></Footer>
          </div>
          <div id="btnContainer" className = {light}>
          <button onClick = {toggleMode} id = "darkModeBtn"><img src = {btnImgSrc} alt = "light and dark mode" id = "darkModeImg" /></button>
          </div>
        </section>
      </BrowserRouter>
    </>
  );
}

export default App