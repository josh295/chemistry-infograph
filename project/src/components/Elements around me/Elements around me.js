import React from 'react';
import { useEffect } from 'react';
import NElement from './NElement.js';
import OElement from './OElement.js';
import ArElement from './ArElement.js';
import AroundMeHeader from './Around Me Header.js';

function ElementsAroundMe() {
    useEffect(() => {
        document.title = "Elements Around Me";
    });

    return (
        <>
        <div>
            <AroundMeHeader />
            <NElement />
            <OElement />
            <ArElement />
        </div>
        </>
    )
}

export default ElementsAroundMe;
