import React from 'react';
import "../Header.css"

function AroundMeHeader() {

    return (
        <>
            <h1 id = "title">Elements Around Me</h1>
            <p id = "intro">Countless elements are present around us and we interact with them in our daily lives. Some of these elements are crucial for all humans. These elements include:</p>
            <div id = "elements">
                <p><a href = "#NCont">Nitrogen</a></p>
                <p><a href = "#OCont">Oxygen</a></p>
                <p><a href = "#ArCont">Argon</a></p>
            </div>
            <div id = "elementsImg">
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305933%2Fperiodic_table_nitrogen_tile_coaster.jpg%3Fheight%3D630%26width%3D630%26qv%3D90&f=1&nofb=1" alt="nitrogen in periodic table" />
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305922%2Fperiodic_table_oxygen_tile_coaster.jpg%3Fside%3DFront%26height%3D630%26width%3D630%26qv%3D90&f=1&nofb=1" alt="oxygen in periodic table" />
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi0.wp.com%2Fhighnames.com%2Fapp%2Fuploads%2F2018%2F09%2Fargon-periodic-table-naming-argentina.jpg%3Fssl%3D1&f=1&nofb=1" alt="argon in periodic table" />
            </div>
            <hr id = "line"/>
        </>
    )
}

export default AroundMeHeader
