import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function OElement() {
    const OElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>4</sup></p>;
    const gramsPerCubicCentimeter = <p>0.001429 g cm<sup>-3</sup></p>;
    const OCompounds = <p>Ozone (O<sub>3</sub>), a highly active compound, is formed by the action of an electrical discharge or ultraviolet light on oxygen. Ozone's presence in the atmosphere (amounting to the equivalent of a layer 3 mm thick under ordinary pressures and temperatures) helps prevent harmful ultraviolet rays of the sun from reaching the earth's surface. Pollutants in the atmosphere may have a detrimental effect on this ozone layer. Ozone is toxic and exposure should not exceed 0.2 mg/m# (8-hour time-weighted average - 40-hour work week). Undiluted ozone has a bluish color. Liquid ozone is bluish black and solid ozone is violet-black.</p>
    const source = <p>Oxygen is the third most abundant element found in the sun, and it plays a part in the carbon-nitrogen cycle, the process once thought to give the sun and stars their energy. A gaseous element, oxygen forms 21% of the atmosphere by volume and is obtained by liquefaction and fractional distillation. The atmosphere of Mars contains about 0.15% oxygen. The element and its compounds make up 49.2%, by weight, of the earth's crust. About two thirds of the human body and nine tenths of water is oxygen.

In the laboratory it can be prepared by the electrolysis of water or by heating potassium chlorate with manganese dioxide as a catalyst.</p>

    const O = {
    name : "Oxygen",
    elementSymbol: "O",
    protonNum : "8",
    electronicConfiguration: OElectronConfig,
    group: "16",
    period: "2",
    density: gramsPerCubicCentimeter,
    meltingPoint: "54.36 K (-218.79°C or -361.82°F)",
    boilingPoint: "90.20 K (-182.95°C or -297.31°F)",
    atomicMass: "15.9994",
    ionisationEnergy: "13.618 eV",
    oxidationStates: "-2",
    sources: source,
    compounds: OCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = O;

    //scroll animation

    const [show , setShow] = useState ({OShow : false});
    const ORef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const OPos = topPos(ORef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (OPos < scrollPos) {
                setShow(state => ({ OShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <OContainer animate = {show.OShow} ref = {ORef} id = "OCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305922%2Fperiodic_table_oxygen_tile_coaster.jpg%3Fside%3DFront%26height%3D630%26width%3D630%26qv%3D90&f=1&nofb=1" alt="O in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fuseruploads.socratic.org%2FFP08LXCnTNK39AwhobDS_electron-dot-diagram-for-oxygen-excellent-design.png&f=1&nofb=1" alt="O Lewis structure"/><figcaption>Oxygen atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Required by most living organisms and for most forms of combustion</li>
                    <li>Impurities in molten pig iron are burned away with streams of high pressure oxygen to produce steel</li>
                    <li>Can be combined with acetylene (C2H2) to produce an extremely hot flame used for welding</li>
                    <li>Liquid oxygen, when combined with liquid hydrogen, makes an excellent rocket fuel</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </OContainer>
        </>
    );
}

    const OContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default OElement
