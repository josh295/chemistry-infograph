import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function NElement() {
    const NElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>3</sup></p>;
    const gramsPerCubicCentimeter = <p>0.0012506 g cm<sup>-3</sup></p>;
    const NCompounds = <p>Sodium nitrate (NaNO<sub>3</sub>) and potassium nitrate (KNO<sub>3</sub>) are formed by the decomposition of organic matter with compounds of these metals present. In certain dry areas of the world these saltpeters are found in quantity and are used as fertilizers. 
    Other inorganic nitrogen compounds are nitric acid (HNO<sub>3</sub>), ammonia (NH<sub>3</sub>), the oxides (NO, NO<sub>2</sub>, N<sub>2</sub>O<sub>4</sub>, N<sub>2</sub>O), cyanides (CN-), etc.</p>
    const source = <p>Nitrogen gas (N<sub>2</sub>) makes up 78.1% of the Earth’s air, by volume. The atmosphere of Mars, by comparison, is only 2.6% nitrogen. From an exhaustible source in our atmosphere, nitrogen gas can be obtained by liquefaction and fractional distillation. Nitrogen is found in all living systems as part of the makeup of biological compounds.</p>

    const N = {
    name : "Nitrogen",
    elementSymbol: "N",
    protonNum : "20",
    electronicConfiguration: NElectronConfig,
    group: "15",
    period: "2",
    density: gramsPerCubicCentimeter,
    meltingPoint: "63.15 K (-210.00°C or -346.00°F)",
    boilingPoint: "77.36 K (-195.79°C or -320.44°F)",
    atomicMass: "14.00674",
    ionisationEnergy: "14.534 eV",
    oxidationStates: "+5, +4, +3, +2, +1, -1, -2, -3",
    sources: source,
    compounds: NCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = N;

    //scroll animation

    const [show , setShow] = useState ({NShow : false});
    const NRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const NPos = topPos(NRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (NPos < scrollPos) {
                setShow(state => ({ NShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <NContainer animate = {show.NShow} ref = {NRef} id = "NCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305933%2Fperiodic_table_nitrogen_tile_coaster.jpg%3Fheight%3D630%26width%3D630%26qv%3D90&f=1&nofb=1" alt="N in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Frayschemworld.pbworks.com%2Ff%2F1349966207%2FNitrogenLewisDot.GIF&f=1&nofb=1" alt="N Lewis structure"/><figcaption>Nitrogen atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Production of ammonia (NH<sub>3</sub>). Large amounts of nitrogen are combined with hydrogen to produce ammonia in a method known as the Haber process</li>
                    <li>Nitrogen gas is largely inert and is used as a protective shield in the semiconductor industry and during certain types of welding and soldering operations</li>
                    <li>Oil companies use high pressure nitrogen to help force crude oil to the surface</li>
                    <li>Liquid nitrogen is an inexpensive cryogenic liquid used for refrigeration, preservation of biological samples and for low temperature scientific experimentation</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </NContainer>
        </>
    );
}

    const NContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default NElement
