import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function ArElement() {
    const ArElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>6</sup></p>;
    const gramsPerCubicCentimeter = <p>0.0017837 g cm<sup>-3</sup></p>;
    const ArCompounds = <p>Argon was once thought to be completely inert. It does not form compounds with any other element or atom. It has already achieved a stable octet electronic configuration with fully filled outer shell. Therefore, it cannot donate, receive or share electrons. However, argon is known to form at least one compound. The synthesis of argon fluorohydride (HArF) was reported by Leonid Khriachtchev, Mika Pettersson, Nino Runeberg, Jan Lundell and Markku Räsänen in August of 2000. Stable only at very low temperatures, argon fluorohydride begins to decompose once it warms above -246°C (-411°F). Because of this limitation, argon fluorohydride has no uses outside of basic scientific research.</p>
    const source = <p>The gas is prepared by fractionation of liquid air because the atmosphere contains 0.94% argon. The atmosphere of Mars contains 1.6% of <sup>40</sup>Ar and 5 ppm of <sup>36</sup>Ar.</p>

    const Ar = {
    name : "Argon",
    elementSymbol: "Ar",
    protonNum : "20",
    electronicConfiguration: ArElectronConfig,
    group: "18",
    period: "3",
    density: gramsPerCubicCentimeter,
    meltingPoint: "83.80 K (-189.35°C or -308.83°F)",
    boilingPoint: "87.30 K (-185.85°C or -302.53°F)",
    atomicMass: "39.948",
    ionisationEnergy: "15.760 eV",
    oxidationStates: "0",
    sources: source,
    compounds: ArCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = Ar;

    //scroll animation

    const [show , setShow] = useState ({ArShow : false});
    const ArRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const ArPos = topPos(ArRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (ArPos < scrollPos) {
                setShow(state => ({ ArShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <ArContainer animate = {show.ArShow} ref = {ArRef} id = "ArCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi0.wp.com%2Fhighnames.com%2Fapp%2Fuploads%2F2018%2F09%2Fargon-periodic-table-naming-argentina.jpg%3Fssl%3D1&f=1&nofb=1" alt="Ar in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fe%2Fee%2FArgon_Lewis_Dot_Diagram.svg%2F600px-Argon_Lewis_Dot_Diagram.svg.png&f=1&nofb=1" alt="Ar Lewis structure"/><figcaption>Argon atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Used to fill incandescent and fluorescent light bulbs to prevent oxygen from corroding the hot filament</li>
                    <li>Used to form inert atmospheres for arc welding, growing semiconductor crystals and processes that require shielding from other atmospheric gases</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </ArContainer>
        </>
    );
}

    const ArContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default ArElement
