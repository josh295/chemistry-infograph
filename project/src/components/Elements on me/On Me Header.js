import React from 'react';
import "../Header.css"

function OnMeHeader() {

    return (
        <>
            <h1 id = "title">Elements On Me</h1>
            <p id = "intro">We may have several elements on our body that we do not even realise are there. The elements are inside mostly things we wear. For instance, spectacles, jewellery and clothing. Examples for some of these elements are:</p>
            <div id = "elements">
                <p><a href = "#SiCont">Silicon</a></p>
                <p><a href = "#TiCont">Titanium</a></p>
                <p><a href = "#AgCont">Silver</a></p>
            </div>
            <div id = "elementsImg">
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305992%2Fperiodic_table_silicon_tile_coaster.jpg%3Fside%3DFront%26height%3D460%26width%3D460%26qv%3D90&f=1&nofb=1" alt="silicon in periodic table" />
                <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fmerchandise%2F37_550x550_Front_Color-NA.jpg%3FAttributeValue%3DNA%26c%3DTrue%26region%3D%257B%2522name%2522%3A%2522FrontCenter%2522%2C%2522width%2522%3A4.25%2C%2522height%2522%3A4.25%2C%2522alignment%2522%3A%2522MiddleCenter%2522%2C%2522orientation%2522%3A0%2C%2522dpi%2522%3A200%2C%2522crop_x%2522%3A0%2C%2522crop_y%2522%3A0%2C%2522crop_h%2522%3A800%2C%2522crop_w%2522%3A800%2C%2522scale%2522%3A0%2C%2522template%2522%3A%257B%2522id%2522%3A92038431%2C%2522params%2522%3A%257B%257D%257D%257D&f=1&nofb=1" alt="titanium in periodic table" />
                <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305957%2Fperiodic_table_silver_tile_coaster.jpg%3Fheight%3D225%26width%3D225&f=1&nofb=1" alt="silver in periodic table" />
            </div>
            <hr id = "line"/>
        </>
    )
}

export default OnMeHeader
