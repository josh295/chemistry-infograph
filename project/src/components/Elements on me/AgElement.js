import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function AgElement() {
    const AgElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>6</sup> 3d<sup>10</sup> 4s<sup>2</sup> 4p<sup>6</sup> 4d<sup>10</sup> 5s<sup>1</sup></p>;
    //1s2 2s2 2p6 3s2 3p6 3d10 4s2 4p6 4d10 5s1
    const gramsPerCubicCentimeter = <p>10.501 g cm<sup>-3</sup></p>;
    const AgCompounds = <p>Sterling silver, an alloy containing 92.5% silver, is used to make silverware, jewelry and other decorative items. 
    High capacity batteries can be made with silver and zinc and silver and cadmium. 
    Silver nitrate (AgNO3) is light sensitive and is used to make photographic films and papers. 
    Silver iodide (AgI) is used to seed clouds to produce rain.
    Silver chloride has interesting optical properties as it can be made transparent; it also is a cement for glass.</p>
    const source = <p>Silver occurs natively and in ores such as argentite (Ag<sub>2</sub>S) and horn silver (AgCl); lead, lead-zinc, copper, gold, and copper-nickel ores are principal sources. Silver is also recovered during electrolytic refining of copper. Commercial fine silver contains at least 99.9% silver. Purities of 99.999+% are available commercially.</p>

    const Ag = {
    name : "Silver",
    elementSymbol: "Ag",
    protonNum : "47",
    electronicConfiguration: AgElectronConfig,
    group: "11",
    period: "5",
    density: gramsPerCubicCentimeter,
    meltingPoint: "1234.93 K (961.78°C or 1763.20°F)",
    boilingPoint: "2435 K (2162°C or 3924°F)",
    atomicMass: "107.8682",
    ionisationEnergy: "7.576 eV",
    oxidationStates: "+1",
    sources: source,
    compounds: AgCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = Ag;

    //scroll animation

    const [show , setShow] = useState ({AgShow : false});
    const AgRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const AgPos = topPos(AgRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (AgPos < scrollPos) {
                setShow(state => ({ AgShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <AgContainer animate = {show.AgShow} ref = {AgRef} id = "AgCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305957%2Fperiodic_table_silver_tile_coaster.jpg%3Fheight%3D225%26width%3D225&f=1&nofb=1" alt="Ag in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fsilver2015.weebly.com%2Fuploads%2F4%2F4%2F6%2F1%2F44612573%2F4682376.png&f=1&nofb=1" alt="Ag Lewis structure"/><figcaption>Silver atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Pure silver is the best conductor of heat and electricity of all known metals, so it is sometimes used in making solder, electrical contacts and printed circuit boards.</li>
                    <li>Silver is also the best reflector of visible light known, but silver mirrors must be given a protective coating to prevent them from tarnishing</li>
                    <li>Has been used to create coins</li>
                    <li>Silver paints are used for making printed circuits</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </AgContainer>
        </>
    );
}

    const AgContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default AgElement
