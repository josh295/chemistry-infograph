import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function SiElement() {
    const SiElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>2</sup></p>;
    const gramsPerCubicCentimeter = <p>2.3296 g cm<sup>-3</sup></p>;
    const SiCompounds = <p>Silicon dioxide (SiO<sub>2</sub>), silicon's most common compound, is the most abundant compound in the earth's crust. It commonly takes the form of ordinary sand, but also exists as quartz, rock crystal, amethyst, agate, flint, jasper and opal. Silicon dioxide is extensively used in the manufacture of glass and bricks. Silica gel, a colloidal form of silicon dioxide, easily absorbs moisture and is used as a desiccant.
    Silicon forms other useful compounds. Silicon carbide (SiC) is nearly as hard as diamond and is used as an abrasive. 
    Sodium silicate (Na<sub>2</sub>SiO<sub>3</sub>), also known as water glass, is used in the production of soaps, adhesives and as an egg preservative. 
    Silicon tetrachloride (SiCl<sub>4</sub>) is used to create smoke screens. 
    Silicon is also an important ingredient in silicone, a class of material that is used for such things as lubricants, polishing agents, electrical insulators and medical implants.
    </p>
    const source = <p>Silicon is present in the sun and stars and is a principal component of a class of meteorites known as aerolites. It is also a component of tektites, a natural glass of uncertain origin. Silicon makes up 25.7% of the earth's crust, by weight, and is the second most abundant element, being exceeded only by oxygen. Silicon is not found free in nature, but occurs chiefly as the oxide and as silicates. Sand, quartz, rock crystal, amethyst, agate, flint, jasper, and opal are some of the forms in which the oxide appears. Granite, hornblende, asbestos, feldspar, clay, mica, etc. are but a few of the numerous silicate minerals. Silicon is prepared commercially by heating silica and carbon in an electric furnace, using carbon electrodes. Several other methods can be used for preparing the element. Amorphous silicon can be prepared as a brown powder, which can be easily melted or vaporized. The Czochralski process is commonly used to produce single crystals of silicon used for solid-state or semiconductor devices. Hyperpure silicon can be prepared by the thermal decomposition of ultra-pure trichlorosilane in a hydrogen atmosphere, and by a vacuum float zone process.</p>

    const Si = {
    name : "Silicon",
    elementSymbol: "Si",
    protonNum : "14",
    electronicConfiguration: SiElectronConfig,
    group: "14",
    period: "3",
    density: gramsPerCubicCentimeter,
    meltingPoint: "1687 K (1414°C or 2577°F)",
    boilingPoint: "3538 K (3265°C or 5909°F)",
    atomicMass: "28.0855",
    ionisationEnergy: "8.152 eV",
    oxidationStates: "+4, +2, -4",
    sources: source,
    compounds: SiCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = Si;

    //scroll animation

    const [show , setShow] = useState ({SiShow : false});
    const SiRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const SiPos = topPos(SiRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (SiPos < scrollPos) {
                setShow(state => ({ SiShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <SiContainer animate = {show.SiShow} ref = {SiRef} id = "SiCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fproduct%2F1389305992%2Fperiodic_table_silicon_tile_coaster.jpg%3Fside%3DFront%26height%3D460%26width%3D460%26qv%3D90&f=1&nofb=1" alt="Si in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstudy.com%2Fcimages%2Fmultimages%2F16%2F24cd2239-d122-45b2-9b43-1570f5e1e6b1_silicon.png&f=1&nofb=1" alt="Si Lewis structure"/><figcaption>Silicon atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>In the form of sand and clay it is used to make concrete and brick</li>
                    <li>In the form of silicates it is used in making enamels, pottery, etc</li>
                    <li>Silica, as sand, is a principal ingredient of glass, one of the most inexpensive of materials with excellent mechanical, optical, thermal, and electrical properties</li>
                    <li>Hyperpure silicon can be doped with boron, gallium, phosphorus, or arsenic to produce silicon for use in transistors, solar cells, rectifiers, and other solid-state devices which are used extensively in the electronics and space-age industries</li>
                    <li>Hydrogenated amorphous silicon has shown promise in producing economical cells for converting solar energy into electricity</li>
                    <li>Diatoms in both fresh and salt water extract Silica from the water to build their cell walls. Silica is present in the ashes of plants and in the human skeleton</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </SiContainer>
        </>
    );
}

    const SiContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default SiElement
