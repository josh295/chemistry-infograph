import React, { useEffect } from 'react';
import SiElement from './SiElement.js';
import TiElement from './TiElement.js';
import AgElement from './AgElement.js';
import OnMeHeader from './On Me Header.js';

function ElementsOnMe() {
    useEffect(() => {
        document.title = "Elements On Me"
    });

    return (
        <>
        <div>
            <OnMeHeader />
            <SiElement />
            <TiElement />
            <AgElement />
        </div>
        </>
    )
}

export default ElementsOnMe;
