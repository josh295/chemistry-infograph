import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function TiElement() {
    const TiElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>6</sup> 3d<sup>2</sup> 4s<sup>2</sup></p>;
    const gramsPerCubicCentimeter = <p>4.5 g cm<sup>-3</sup></p>;
    const TiCompounds = <p>Titanium dioxide is extensively used for both house paint and artist's paint, because it is permanent and has good covering power. Titanium oxide pigment accounts for the largest use of the element. Titanium paint is an excellent reflector of infrared, and is extensively used in solar observatories where heat causes poor viewing conditions. 
    Titanium tetrachloride is used to iridize glass. This compound fumes strongly in air and has been used to produce smoke screens.</p>
    const source = <p>Titanium is present in meteorites and the sun. Rocks obtained during the Apollo 17 lunar mission showed presence of 12.1% TiO<sub>2</sub>; rocks obtained during earlier Apollo missions show lower percentages. Titanium oxide bands are prominent in the spectra of M-type stars. The element is the ninth most abundant in the crust of the earth. Titanium is almost always present in igneous rocks and in the sediments derived from them. It occurs in the minerals rutile, ilmenite, and sphene, and is present in titanates and in many iron ores. Titanium is present in ash of coal, in plants, and in human body. Titanium could also be produced commercially by reducing titanium tetrachloride with magnesium. This method is still largely used for producing the metal. The metal can be purified by decomposing the iodide.</p>

    const Ti = {
    name : "Titanium",
    elementSymbol: "Ti",
    protonNum : "22",
    electronicConfiguration: TiElectronConfig,
    group: "4",
    period: "4",
    density: gramsPerCubicCentimeter,
    meltingPoint: "1941 K (1668°C or 3034°F)",
    boilingPoint: "3560 K (3287°C or 5949°F)",
    atomicMass: "47.867",
    ionisationEnergy: "6.828 eV",
    oxidationStates: "+4, +3, +2",
    sources: source,
    compounds: TiCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = Ti;

    //scroll animation

    const [show , setShow] = useState ({TiShow : false});
    const TiRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const TiPos = topPos(TiRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (TiPos < scrollPos) {
                setShow(state => ({ TiShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <TiContainer animate = {show.TiShow} ref = {TiRef} id = "TiCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi3.cpcache.com%2Fmerchandise%2F37_550x550_Front_Color-NA.jpg%3FAttributeValue%3DNA%26c%3DTrue%26region%3D%257B%2522name%2522%3A%2522FrontCenter%2522%2C%2522width%2522%3A4.25%2C%2522height%2522%3A4.25%2C%2522alignment%2522%3A%2522MiddleCenter%2522%2C%2522orientation%2522%3A0%2C%2522dpi%2522%3A200%2C%2522crop_x%2522%3A0%2C%2522crop_y%2522%3A0%2C%2522crop_h%2522%3A800%2C%2522crop_w%2522%3A800%2C%2522scale%2522%3A0%2C%2522template%2522%3A%257B%2522id%2522%3A92038431%2C%2522params%2522%3A%257B%257D%257D%257D&f=1&nofb=1" alt="Ti in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.chemistrylearner.com%2Fwp-content%2Fuploads%2F2018%2F09%2FTitanium-Lewis-Dot-Structure.jpg&f=1&nofb=1" alt="Ti Lewis structure"/><figcaption>Titanium atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Used in airplanes, missiles and rockets where strength, low weight and resistance to high temperatures are important</li>
                    <li>Since titanium does not react within the human body, it is used to create artificial hips, pins for setting bones and for other biological implants</li>
                    <li>Important as an alloying agent with aluminum, molybdenum, manganese, iron, and other metals</li>
                    <li>Has potential use in desalination plants for converting sea water into fresh water. A titanium anode coated with platinum has been used to provide cathodic protection from corrosion by salt water</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </TiContainer>
        </>
    );
}

    const TiContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default TiElement
