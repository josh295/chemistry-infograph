import React from 'react';
import "./Footer.css";

function Footer() {
    return (
        <>
        <hr id = "footerLine"/>
        <section id = "footerContainer">
            <div id = "creator">
            <p className = "credits">Credits: Joshua Lim Chiew Khoon</p>
            </div>
            <div id = "date">
                <p className = "credits">Created from 21 - 25 Sep 2021</p>
            </div>
        </section>
        </>
    )
}

export default Footer
