import React , { useEffect } from 'react';
import "./Header.css";

function References() {
    useEffect(() => {
        document.title = "References"
    })
    return (
        <>
            <h1 id = "title">References / Bibliography</h1>
            <ul className = "refs">
                <li><a rel = "noreferrer"href="https://pubchem.ncbi.nlm.nih.gov/periodic-table/" target = "_blank">PubChem Periodic Table</a></li>
                <li><a rel = "noreferrer" href="https://periodictableguide.com/electron-configuration-chart-of-all-elements/">Electron Configuration Chart of All Elements</a></li>
            </ul>
        </>
    )
}

export default References
