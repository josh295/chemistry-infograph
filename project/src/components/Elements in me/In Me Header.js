import React from 'react';
import "../Header.css"

function InMeHeader() {

    return (
        <>
            <h1 id = "title">Elements In Me</h1>
            <p id = "intro">There are many elements found in the human body. These elements are needed for various reasons in the body so we can carry out processes that allow us to live and survive. 3 of these mentioned elements are:</p>
            <div id = "elements">
                <p><a href = "#CCont">Carbon</a></p>
                <p><a href = "#CaCont">Calcium</a></p>
                <p><a href = "#KCont">Potassium</a></p>
            </div>
            <div id = "elementsImg">
                <img src="https://i3.cpcache.com/merchandise/37_550x550_Front_Color-NA.jpg?Size=NA&AttributeValue=NA&region=%7B%22name%22:%22FrontCenter%22,%22width%22:4.25,%22height%22:4.25,%22alignment%22:%22MiddleCenter%22,%22orientation%22:0,%22dpi%22:200,%22crop_x%22:0,%22crop_y%22:0,%22crop_h%22:800,%22crop_w%22:800,%22scale%22:0,%22template%22:%7B%22id%22:92037078,%22params%22:%7B%7D%7D%7D" alt="carbon in periodic table" />
                <img src="https://i3.cpcache.com/product/1538208813/20_calcium_tile_coaster.jpg?width=750&height=750&Filters=[%7B%22name%22:%22background%22%2C%22value%22:%22F2F2F2%22%2C%22sequence%22:2%7D]" alt="calcium in periodic table" />
                <img src="https://i3.cpcache.com/product/1537976694/19_potassium_tile_coaster.jpg?width=750&height=750&Filters=[%7B%22name%22:%22background%22%2C%22value%22:%22F2F2F2%22%2C%22sequence%22:2%7D]" alt="potassium in periodic table" />
            </div>
            <hr id = "line"/>
        </>
    )
}

export default InMeHeader
