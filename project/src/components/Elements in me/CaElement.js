import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function CaElement() {
    const CaElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>6</sup> 4s<sup>2</sup></p>;
    const gramsPerCubicCentimeter = <p>1.54 g cm<sup>-3</sup></p>;
    const CaCompounds = <p>Calcium's natural and prepared compounds are widely used. 
    Calcium carbonate (CaCO<sub>3</sub>) is one of the common compounds of calcium. Chalk, marble and limestone are all forms of calcium carbonate. Calcium carbonate is also used to make white paint, cleaning powder, toothpaste and stomach antacids, among other things. 
    It is heated to form quicklime (CaO) which is then added to water (H<sub>2</sub>O). This forms another material known as slaked lime (Ca(OH)<sub>2</sub>) which is an inexpensive base material used throughout the chemical industry. When mixed with sand, it hardens mortar and plaster by taking up carbon dioxide from the air. Calcium from limestone is an important element in Portland cement.Solubility of the carbonate in water containing carbon dioxide is high, which causes the formation of caves with stalactites and stalagmites and is responsible for hardness in water. 
    Other common compounds of calcium include: calcium sulfate (CaSO<sub>4</sub>), also known as gypsum, which is used to make dry wall and plaster of Paris, calcium nitrate (Ca(NO<sub>3</sub>)<sub>2</sub>), a naturally occurring fertilizer and calcium phosphate (Ca<sub>3</sub>(PO<sub>4</sub>)<sub>2</sub>), the main material found in bones and teeth. Other important compounds are the carbide, chloride, cyanamide, hypochlorite, nitrate, and sulfide.</p>
    const source = <p>Calcium, a metallic element, is fifth in abundance in the earth's crust, of which it forms more than 3%. It is an essential constituent of leaves, bones, teeth, and shells. Never found in nature uncombined, it occurs abundantly as limestone, gypsum, and fluorite. Apatite is the fluorophosphate or chlorophosphate of calcium.</p>

    const Ca = {
    name : "Calcium",
    elementSymbol: "Ca",
    protonNum : "20",
    electronicConfiguration: CaElectronConfig,
    group: "2",
    period: "4",
    density: gramsPerCubicCentimeter,
    meltingPoint: "1115 K (842°C or 1548°F)",
    boilingPoint: "1757 K (1484°C or 2703°F)",
    atomicMass: "40.078",
    ionisationEnergy: "6.113 eV",
    oxidationStates: "+2",
    sources: source,
    compounds: CaCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = Ca;

    //scroll animation

    const [show , setShow] = useState ({CaShow : false});
    const CaRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const CaPos = topPos(CaRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (CaPos < scrollPos) {
                setShow(state => ({ CaShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <CaContainer animate = {show.CaShow} ref = {CaRef} id = "CaCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://i3.cpcache.com/product/1538208813/20_calcium_tile_coaster.jpg?width=750&height=750&Filters=[%7B%22name%22:%22background%22%2C%22value%22:%22F2F2F2%22%2C%22sequence%22:2%7D]" alt="Ca in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F4%2F4c%2FLewis_dot_Ca.svg%2F480px-Lewis_dot_Ca.svg.png&f=1&nofb=1" alt="Ca Lewis structure"/><figcaption>Calcium atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Used in some chemical processes to refine thorium, uranium and zirconium</li>
                    <li>Used to remove oxygen, sulfur and carbon from certain alloys</li>
                    <li>Can be alloyed with aluminum, beryllium, copper, lead and magnesium</li>
                    <li>Used in vacuum tubes as a getter, a material that combines with and removes trace gases from vacuum tubes</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </CaContainer>
        </>
    );
}

    const CaContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default CaElement
