import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function CElement() {
    const CElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>2</sup></p>;
    const gramsPerCubicCentimeter = <p>2.2670 g cm<sup>-3</sup></p>;
    const CCompounds = <p>Carbon is found free in nature in three allotropic forms: graphite, diamond, and fullerines. A fourth form, known as "white" carbon, is now thought to exist. 
    Graphite is one of the softest known materials while diamond is one of the hardest. Graphite exists in two forms: alpha and beta. These have identical physical properties, except for their crystal structure. Naturally occurring graphites are reported to contain as much as 30% of the rhombohedral (beta) form, whereas synthetic materials contain only the alpha form. The hexagonal alpha type can be converted to the beta by mechanical treatment, and the beta form reverts to the alpha on heating it above 1000°C. 
    In combination, carbon is found as carbon dioxide in the atmosphere of the earth and dissolved in all natural waters. It is a component of great rock masses in the form of carbonates of calcium (limestone), magnesium, and iron. Coal, petroleum, and natural gas are chiefly hydrocarbons.
    Other compounds of carbon are carbon dioxide (CO<sub>2</sub>), carbon monoxide (CO), carbon disulfide (CS<sub>2</sub>), chloroform (CHCl<sub>3</sub>), carbon tetrachloride (CCl<sub>4</sub>), methane (CH<sub>4</sub>), ethylene (C<sub>2</sub>H<sub>4</sub>), acetylene (C<sub>2</sub>H<sub>2</sub>), benzene (C<sub>6</sub>H<sub>6</sub>), acetic acid (CH<sub>3</sub>COOH), and their derivatives.</p>;
    const source = <p>Carbon, the sixth most abundant element in the universe, has been known since ancient times. Carbon is most commonly obtained from coal deposits, although it usually must be processed into a form suitable for commercial use.</p>

    const C = {
    name : "Carbon",
    elementSymbol: "C",
    protonNum : "6",
    electronicConfiguration: CElectronConfig,
    group: "14",
    period: "2",
    density: gramsPerCubicCentimeter,
    meltingPoint: "3823 K (3550°C or 6422°F)",
    boilingPoint: "4098 K (3825°C or 6917°F)",
    atomicMass: "12.0107",
    ionisationEnergy: "11.260 eV",
    oxidationStates: "+4, +2, -4",
    sources: source,
    compounds: CCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates , sources , compounds} = C;

    //scroll animation

    const [show , setShow] = useState ({CShow : false});
    const CRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const CPos = topPos(CRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (CPos < scrollPos) {
                setShow(state => ({ CShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <CContainer animate = {show.CShow} ref = {CRef} id = "CCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://i3.cpcache.com/merchandise/37_550x550_Front_Color-NA.jpg?Size=NA&AttributeValue=NA&region=%7B%22name%22:%22FrontCenter%22,%22width%22:4.25,%22height%22:4.25,%22alignment%22:%22MiddleCenter%22,%22orientation%22:0,%22dpi%22:200,%22crop_x%22:0,%22crop_y%22:0,%22crop_h%22:800,%22crop_w%22:800,%22scale%22:0,%22template%22:%7B%22id%22:92037078,%22params%22:%7B%7D%7D%7D" alt="C in periodic table" />
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F5%2F5b%2FCarbon_Lewis_Structure_PNG.png%2F788px-Carbon_Lewis_Structure_PNG.png&f=1&nofb=1" alt="C Lewis structure"/><figcaption>Carbon atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Amorphous carbon is formed when a material containing carbon is burned without enough oxygen for it to burn completely. This black soot, also known as lampblack, gas black, channel black or carbon black, is used to make inks, paints and rubber products. It can also be pressed into shapes and is used to form the cores of most dry cell batteries, among other things.</li>
                    <li>Graphite, one of the softest materials known, is a form of carbon that is primarily used as a lubricant. </li>
                    <li>Graphite, in a form known as coke, is used in large amounts in the production of steel.</li>
                    <li>Graphite is also used as the black material in pencils commonly known as lead.</li>
                    <li>Diamond, the third naturally occurring form of carbon, is one of the hardest substances known. Naturally occurring diamond is typically used for jewellery.</li>
                    <li>A fourth allotrope of carbon, known as white carbon is a transparent material that can split a single beam of light into two beams, a property known as birefringence.</li>
                    <li>Large molecules consisting only of carbon, known as buckminsterfullerenes, or buckyballs, have recently been discovered. They can trap other atoms within their framework, appear to be capable of withstanding great pressures and have magnetic and superconductive properties.</li>
                    <li>Carbon-14, a radioactive isotope of carbon with a half-life of 5,730 years, is used to find the age of formerly living things through a process known as radiocarbon dating.</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </CContainer>
        </>
    );
}

    const CContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default CElement