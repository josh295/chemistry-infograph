import React, { useLayoutEffect, useRef, useState } from "react";
import styled from "styled-components";
import "../Elements.css";

function KElement() {
    const KElectronConfig = <p>1s<sup>2</sup> 2s<sup>2</sup> 2p<sup>6</sup> 3s<sup>2</sup> 3p<sup>6</sup> 4s<sup>2</sup></p>;
    const gramsPerCubicCentimeter = <p>0.89 g cm<sup>-3</sup></p>;
    const KCompounds = <p>Potassium forms many important compounds. 
    Potassium forms an alloy with sodium (NaK) that is used as a heat transfer medium in some types of nuclear reactors. 
    Potassium chloride (KCl) is the most common potassium compound. It is used in fertilizers, as a salt substitute and to produce other chemicals. 
    Potassium hydroxide (KOH) is used to make soaps, detergents and drain cleaners. 
    Potassium carbonate (KHCO<sub>3</sub>), also known as pearl ash, is used to make some types of glass and soaps and is obtained commercially as a byproduct of the production of ammonia. 
    Potassium superoxide (KO<sub>2</sub>) can create oxygen from water vapor (H<sub>2</sub>O) and carbon dioxide (CO<sub>2</sub>) through the following reaction: 2KO<sub>2</sub> + H<sub>2</sub>O + 2CO<sub>2</sub> &rarr; 2KHCO<sub>3</sub> + O<sub>2</sub>. It is used in respiratory equipment and is produced by burning potassium metal in dry air. 
    Potassium nitrate (KNO<sub>3</sub>), also known as saltpeter or nitre, is used in fertilizers, match heads and pyrotechnics.</p>
    const source = <p>The metal is the seventh most abundant and makes up about 2.4% by weight of the earth's crust. Most potassium minerals are insoluble and the metal is obtained from them only with great difficulty.Certain minerals, however, such as sylvite, carnallite, langbeinite, and polyhalite are found in ancient lake and sea beds and form rather extensive deposits from which potassium and its salts can readily be obtained. Potassium is also found in the ocean, but is present only in relatively small amounts, compared to sodium.</p>

    const K = {
    name : "Potassium",
    elementSymbol: "K",
    protonNum : "20",
    electronicConfiguration: KElectronConfig,
    group: "1",
    period: "4",
    density: gramsPerCubicCentimeter,
    meltingPoint: "336.53 K (63.38°C or 146.08°F)",
    boilingPoint: "1032 K (759°C or 1398°F)",
    atomicMass: "39.0983",
    ionisationEnergy: "4.341 eV",
    oxidationStates: "+1",
    sources: source,
    compounds: KCompounds
    };
    const {name , elementSymbol , protonNum , electronicConfiguration , group , period , density , meltingPoint , boilingPoint , atomicMass, ionisationEnergy , oxidationStates ,  sources , compounds} = K;

    //scroll animation

    const [show , setShow] = useState ({KShow : false});
    const KRef = useRef (null);

    useLayoutEffect(() => {
        const topPos = element => element.getBoundingClientRect().top;
        const KPos = topPos(KRef.current);

        const onScroll = () => {
            const scrollPos = window.scrollY + window.innerHeight;
            if (KPos < scrollPos) {
                setShow(state => ({ KShow: true }));
            }
        };

        window.addEventListener ("scroll" , onScroll);
        return () => window.removeEventListener("scroll", onScroll);
  }, []);

    return (
        <>
        <KContainer animate = {show.KShow} ref = {KRef} id = "KCont">
            <section id = "Imgs">
                <div id = "Periodic">
                    <img src="https://i3.cpcache.com/product/1537976694/19_potassium_tile_coaster.jpg?width=750&height=750&Filters=[%7B%22name%22:%22background%22%2C%22value%22:%22F2F2F2%22%2C%22sequence%22:2%7D]" alt="Potassium in periodic table"/>
                </div>
                <div id = "Lewis">
                    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fdr282zn36sxxg.cloudfront.net%2Fdatastreams%2Ff-d%3A40367510c25719ca902be28d7e51250ba1252416baeb573df8899b4a%252BIMAGE%252BIMAGE.1&f=1&nofb=1" alt="K Lewis structure"/><figcaption>Potassium atom Lewis structure</figcaption>
                </div>
            </section>
            <section id = "Properties">
                <p>Name: {name}</p>
                <p>Element Symbol: {elementSymbol}</p>
                <p>Proton Number: {protonNum}</p>
                <p>Electronic configuration: {electronicConfiguration}</p>
                <p>Group: {group}</p>
                <p>Period: {period}</p>
                <p>Density: {density}</p>
                <p>Melting Point: {meltingPoint}</p>
                <p>Boiling Point: {boilingPoint}</p>
                <p>Atomic Mass: {atomicMass}</p>
                <p>Ionisation Energy: {ionisationEnergy}</p>
                <p>Oxidation States: {oxidationStates}</p>
                <p>Sources: <br/>{sources}</p>
                <p>Uses: </p>
                <ul>
                    <li>Only have uses for human body</li>
                    <li>Used to remove oxygen, sulfur and Krbon from certain alloys</li>
                    <li>Kn be alloyed with aluminum, beryllium, copper, lead and magnesium</li>
                    <li>Used in vacuum tubes as a getter, a material that combines with and removes trace gases from vacuum tubes</li>
                </ul>
                <p>Compounds: <br/>{compounds}</p>
            </section>
        </KContainer>
        </>
    );
}

    const KContainer = styled.article `
    transform: translateX(${({ animate }) => (animate? "0" : "-100vw")});
    transition: transform 4s;
    `

export default KElement
