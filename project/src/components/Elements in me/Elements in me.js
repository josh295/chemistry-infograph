import React, { useEffect } from 'react';
import CElement from './CElement.js';
import CaElement from './CaElement.js';
import KElement from './KElement.js';
import InMeHeader from './In Me Header.js';


function ElementsInMe() {
    useEffect(() => {
        document.title = "Elements In Me";
    });
    
    return (
        <>
        <div>
            <InMeHeader />
            <CElement />
            <CaElement />
            <KElement />
        </div>
        </>
    )
}

export default ElementsInMe;
