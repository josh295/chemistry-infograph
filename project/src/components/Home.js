import React , { useEffect } from 'react';
import "./Home.css";

function Home() {
    useEffect(() => {
        document.title = "Home";
    })

    return (
        <>
        <div>
            <h1 id = "wlc">Welcome!</h1>
            <h2 id = "descp">This is a simple site to illustrate my chemistry infograph for the first semester assignment in matriculation</h2>
            <h3 id = "sideNav">Please navigate to specific parts of the site through the navigation tab on the left side</h3>
            <h1 id = "enjoy">Hope you enjoy!</h1>
        </div>
        </>
    )
}

export default Home