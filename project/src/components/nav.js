import React from 'react';
import { useState } from 'react';
import {Link} from "react-router-dom";
import "./nav.css";

function Nav() {
    const imageSrc = "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Hamburger_icon.svg/1200px-Hamburger_icon.svg.png";
    const [nav, setNav] = useState ("nav");

    const homebtn = <Link to = "/" className = "navSelection" >Home</Link>;
    const InMebtn = <Link to = "/ElementsInMe" className = "navSelection" >Elements In Me</Link>;
    const OnMebtn = <Link to = "/ElementsOnMe" className = "navSelection" >Elements On Me</Link>;
    const AroundMebtn = <Link to = "/ElementsAroundMe" className = "navSelection" >Elements Around Me</Link>;
    const RefBtn = <Link to = "/References" className = "navSelection">References</Link>

    const hide = () => {
        if (nav === "navHidden") {
            setNav ("nav");
        }
    }

    const show = () => {
        if (nav === "nav"){
        setNav ("navHidden");
        }
    }

    const hideAndShow = () => {
        if (nav === "nav") {
            setNav ("navHidden");
        }
        else if (nav === "navHidden"){
            setNav ("nav");
        }
    }


    return (
        <>
            <div id = "navSection">
                <nav onMouseEnter = {show} onMouseLeave = {hide} className = {nav} style = {{height: "100vh"}}>
                    {homebtn}
                    {InMebtn}
                    {OnMebtn}
                    {AroundMebtn}
                    {RefBtn}
                </nav>
                <button onClick = {hideAndShow} className = "toggleBtn"><img src = {imageSrc} alt = "menu toggle pic" className = "toggleImg"/></button>
            </div>
        </>
    )

}

export default Nav;
